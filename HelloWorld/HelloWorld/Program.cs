﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

//Unused Using

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        //Unused method
        public static void Looping()
        {
            while (true)
            {
                //Infinite looping
            }

            return; //Unreachable 
        }

        public static int NullIt()
        {
            var ints = new List<int>();

            return ints.First();
        }
    }
}
